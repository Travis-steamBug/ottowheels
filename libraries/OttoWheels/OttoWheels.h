#ifndef OttoWheels_h
#define OttoWheels_h

#include <MotorDriver.h>
#include <Oscillator.h>
#include <EEPROM.h>

#include <US.h>
#include <LedMatrix.h>
#include <BatReader.h>

#include "Otto_mouths.h"
#include "Otto_sounds.h"
#include "Otto_gestures.h"


//-- Constants
#define LEFT        1
#define RIGHT       -1
#define SMALL       5
#define MEDIUM      15
#define BIG         30


// TODO: fix these to use the A0-A7 (digital version) pins
#define PIN_Buzzer  A3
#define PIN_Trigger A1
#define PIN_Echo    A0
#define PIN_NoiseSensor A4

#define STEP_TO_SPEED_COEFF 75000.0

class OttoWheels
{
  public:

    //-- Otto initialization
    void init(int motorLeft, int motorRight, bool load_calibration=true, int NoiseSensor=PIN_NoiseSensor, int Buzzer=PIN_Buzzer, int USTrigger=PIN_Trigger, int USEcho=PIN_Echo);

    //-- Attach & detach functions
    void attachMotors();

    //-- Predetermined Motion Functions
    void _moveMotors(int time, int  motor_speeds[]);

    //-- HOME = Otto at rest position
    void home();
    bool getRestState();
    void setRestState(bool state);
    
    //-- Predetermined Motion Functions
    void jump(float steps=1, int T = 2000);

    void walk(float steps=4, int T=1000, int dir = FORWARD);
    void turn(float steps=4, int T=2000, int dir = LEFT);
    void bend (int steps=1, int T=1400, int dir=LEFT);
    void shakeLeg (int steps=1, int T = 2000, int dir=RIGHT);

    bool look(int steps=2, int T=1000, int dir=RIGHT, int minDistance=10, int maxDistance=20);

    void updown(float steps=1, int T=1000, int h = 20);
    void swing(float steps=1, int T=1000, int h=20);
    void tiptoeSwing(float steps=1, int T=900, int h=20);
    void jitter(float steps=1, int T=500, int h=20);
    void ascendingTurn(float steps=1, int T=900, int h=20);

    void moonwalker(float steps=1, int T=900, int h=20, int dir=LEFT);
    void crusaito(float steps=1, int T=900, int h=20, int dir=FORWARD);
    void flapping(float steps=1, int T=1000, int h=20, int dir=FORWARD);

    //-- Sensors functions
    float getDistance(); //US sensor
    int getNoise();      //Noise Sensor

    //-- Battery
    double getBatteryLevel();
    double getBatteryVoltage();
    
    //-- Mouth & Animations
    void putMouth(unsigned long int mouth, bool predefined = true);
    void putAnimationMouth(unsigned long int anim, int index);
    void clearMouth();

    //-- Sounds
    void _tone (float noteFrequency, long noteDuration, int silentDuration);
    void bendTones (float initFrequency, float finalFrequency, float prop, long noteDuration, int silentDuration);
    void sing(int songName);

    //-- Gestures
    void playGesture(int gesture);

 
  private:
    
    LedMatrix ledmatrix;
    BatReader battery;
    MotorDriver motors;
    US us;

    int motor_pins[2];

    int pinBuzzer;
    int pinNoiseSensor;
    
    unsigned long final_time;
    unsigned long partial_time;

    bool isOttoResting;

    unsigned long int getMouthShape(int number);
    unsigned long int getAnimShape(int anim, int index);

};

#endif
