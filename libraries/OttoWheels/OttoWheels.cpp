
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include <pins_arduino.h>
#endif


#include "OttoWheels.h"
#include <US.h>



void OttoWheels::init(int motorLeft, int motorRight, bool load_calibration, int NoiseSensor, int Buzzer, int USTrigger, int USEcho) {
  
  motor_pins[0] = motorLeft;
  motor_pins[1] = motorRight;

  attachMotors();
  isOttoResting=false;

  //US sensor init with the pins:
  us.init(USTrigger, USEcho);

  //Buzzer & noise sensor pins: 
  pinBuzzer = Buzzer;
  pinNoiseSensor = NoiseSensor;

  pinMode(Buzzer,OUTPUT);
  pinMode(NoiseSensor,INPUT);
}

///////////////////////////////////////////////////////////////////
//-- ATTACH FUNCTION --------------------------------------------//
///////////////////////////////////////////////////////////////////
void OttoWheels::attachMotors(){
  motors.motor(motor_pins[0], FORWARD, 0);
	motors.motor(motor_pins[1], FORWARD, 0);
}

///////////////////////////////////////////////////////////////////
//-- BASIC MOTION FUNCTIONS -------------------------------------//
///////////////////////////////////////////////////////////////////
void OttoWheels::_moveMotors(int interval, int motor_speeds[]) {

  attachMotors();
  if(getRestState()==true) {
        setRestState(false);
  }

  unsigned long finalMillis = millis() + interval;

  for (int motor = 0; motor < 2; motor++) {
    int motorDirection = motor_speeds[motor] >= 0 ? FORWARD : BACKWARD;
    motors.motor(motor_pins[motor], motorDirection, abs(motor_speeds[motor]));
  }

  if(interval > 10) {
    for(int loops = 0; millis() < finalMillis; loops++) {
      long loopMillis = millis() + 25;
      while (millis() < loopMillis); // pause
    }
  }

//  if(time>10){
//    for (int i = 0; i < 4; i++) increment[i] = ((motor_target[i]) - servo_position[i]) / (time / 10.0);
//    final_time =  millis() + time;
//
//    for (int iteration = 1; millis() < final_time; iteration++) {
//      partial_time = millis() + 10;
//      for (int i = 0; i < 4; i++) servo[i].SetPosition(motor_target[i] + (iteration * increment[i]));
//      while (millis() < partial_time); //pause
//    }
//  }
//  else{
//    for (int i = 0; i < 4; i++) servo[i].SetPosition(servo_target[i]);
//  }
//  for (int i = 0; i < 4; i++) servo_position[i] = servo_target[i];
}

///////////////////////////////////////////////////////////////////
//-- HOME = Otto at rest position -------------------------------//
///////////////////////////////////////////////////////////////////
void OttoWheels::home(){

  if(isOttoResting==false){ //Go to rest position only if necessary
    
    motors.motor(motor_pins[0], RELEASE, 0);
    motors.motor(motor_pins[1], RELEASE, 0);
    
    isOttoResting=true;
  }
}

bool OttoWheels::getRestState(){

    return isOttoResting;
}

void OttoWheels::setRestState(bool state){

    isOttoResting = state;
}


///////////////////////////////////////////////////////////////////
//-- PREDETERMINED MOTION SEQUENCES -----------------------------//
///////////////////////////////////////////////////////////////////

//---------------------------------------------------------
//-- Otto movement: Jump
//--  Parameters:
//--    steps: Number of steps
//--    T: Period
//---------------------------------------------------------
void OttoWheels::jump(float steps, int T){
  // OttoWheels can't jump
}


//---------------------------------------------------------
//-- Otto gait: Walking  (forward or backward)    
//--  Parameters:
//--    * steps:  Number of steps
//--    * T : Period
//--    * Dir: Direction: FORWARD / BACKWARD
//---------------------------------------------------------
void OttoWheels::walk(float steps, int T, int dir){

  int calculatedSpeed = (int)((steps*STEP_TO_SPEED_COEFF)/(float)T);
  if(calculatedSpeed > 255) {
    calculatedSpeed = 255;
  }

  int speeds[] = {0,0};
  speeds[0] = dir >= 0 ? (calculatedSpeed) : (-calculatedSpeed);
  speeds[1] = dir >= 0 ? (calculatedSpeed) : (-calculatedSpeed);
  _moveMotors(T, speeds);
//  //-- Oscillator parameters for walking
//  //-- Hip sevos are in phase
//  //-- Feet servos are in phase
//  //-- Hip and feet are 90 degrees out of phase
//  //--      -90 : Walk forward
//  //--       90 : Walk backward
//  //-- Feet servos also have the same offset (for tiptoe a little bit)
//  int A[4]= {30, 30, 20, 20};
//  int O[4] = {0, 0, 4, -4};
//  double phase_diff[4] = {0, 0, DEG2RAD(dir * -90), DEG2RAD(dir * -90)};
//
//  //-- Let's oscillate the servos!
//  _execute(A, O, T, phase_diff, steps);  
}


//---------------------------------------------------------
//-- Otto gait: Turning (left or right)
//--  Parameters:
//--   * Steps: Number of steps
//--   * T: Period
//--   * Dir: Direction: LEFT / RIGHT
//---------------------------------------------------------
void OttoWheels::turn(float steps, int T, int dir) {
  
  int calculatedSpeed = (int)((steps*STEP_TO_SPEED_COEFF)/(float)T);
  if(calculatedSpeed > 255) {
    calculatedSpeed = 255;
  }
  
  int speeds[] = {0,0};
  speeds[0] = dir >= 0 ? (calculatedSpeed) : (-calculatedSpeed);
  speeds[1] = dir >= 0 ? (-calculatedSpeed) : (calculatedSpeed);
  _moveMotors(T, speeds);
  
//  //-- Same coordination than for walking (see OttoWheels::walk)
//  //-- The Amplitudes of the hip's oscillators are not igual
//  //-- When the right hip servo amplitude is higher, the steps taken by
//  //--   the right leg are bigger than the left. So, the robot describes an 
//  //--   left arc
//  int A[4]= {30, 30, 20, 20};
//  int O[4] = {0, 0, 4, -4};
//  double phase_diff[4] = {0, 0, DEG2RAD(-90), DEG2RAD(-90)}; 
//    
//  if (dir == LEFT) {  
//    A[0] = 30; //-- Left hip servo
//    A[1] = 10; //-- Right hip servo
//  }
//  else {
//    A[0] = 10;
//    A[1] = 30;
//  }
//    
//  //-- Let's oscillate the servos!
//  _execute(A, O, T, phase_diff, steps); 
}


//---------------------------------------------------------
//-- OttoWheels gait: Lateral bend
//--  Parameters:
//--    steps: Number of bends
//--    T: Period of one bend
//--    dir: RIGHT=Right bend LEFT=Left bend
//---------------------------------------------------------
void OttoWheels::bend (int steps, int T, int dir){
  // OttoWheels can't bend
}


//---------------------------------------------------------
//-- OttoWheels gait: Shake a leg
//--  Parameters:
//--    steps: Number of shakes
//--    T: Period of one shake
//--    dir: RIGHT=Right leg LEFT=Left leg
//---------------------------------------------------------
void OttoWheels::shakeLeg (int steps,int T,int dir){
  // OttoWheels has no legs to shake
}


//---------------------------------------------------------
//-- OttoWheels gait: look in one direction
//--  Parameters:
//--    steps: number of turning increments (1 step ~= 15 degrees?)
//--    T: Period of look
//--    dir: RIGHT=Right leg LEFT=Left leg
//--    minDistance: look for obstruction farther than this
//--    maxDistance: look for obstruction closer than this
//---------------------------------------------------------
bool OttoWheels::look(int steps, int T, int dir, int minDistance, int maxDistance) {
  
  this->getDistance();
  return false;
}



//---------------------------------------------------------
//-- OttoWheels movement: up & down
//--  Parameters:
//--    * steps: Number of jumps
//--    * T: Period
//--    * h: Jump height: SMALL / MEDIUM / BIG 
//--              (or a number in degrees 0 - 90)
//---------------------------------------------------------
void OttoWheels::updown(float steps, int T, int h){
  // OttoWheels can't jump
}


//---------------------------------------------------------
//-- OttoWheels movement: swinging side to side
//--  Parameters:
//--     steps: Number of steps
//--     T : Period
//--     h : Amount of swing (from 0 to 50 aprox)
//---------------------------------------------------------
void OttoWheels::swing(float steps, int T, int h){
  // OttoWheels can't swing
}


//---------------------------------------------------------
//-- OttoWheels movement: swinging side to side without touching the floor with the heel
//--  Parameters:
//--     steps: Number of steps
//--     T : Period
//--     h : Amount of swing (from 0 to 50 aprox)
//---------------------------------------------------------
void OttoWheels::tiptoeSwing(float steps, int T, int h){
  // OttoWheels has no toes
}


//---------------------------------------------------------
//-- OttoWheels gait: Jitter 
//--  Parameters:
//--    steps: Number of jitters
//--    T: Period of one jitter 
//--    h: height (Values between 5 - 25)   
//---------------------------------------------------------
void OttoWheels::jitter(float steps, int T, int h){
//
//  //-- Both feet are 180 degrees out of phase
//  //-- Feet amplitude and offset are the same
//  //-- Initial phase for the right foot is -90, so that it starts
//  //--   in one extreme position (not in the middle)
//  //-- h is constrained to avoid hit the feets
//  h=min(25,h);
//  int A[4]= {h, h, 0, 0};
//  int O[4] = {0, 0, 0, 0};
//  double phase_diff[4] = {DEG2RAD(-90), DEG2RAD(90), 0, 0};
//  
//  //-- Let's oscillate the servos!
//  _execute(A, O, T, phase_diff, steps); 
}


//---------------------------------------------------------
//-- OttoWheels gait: Ascending & turn (Jitter while up&down)
//--  Parameters:
//--    steps: Number of bends
//--    T: Period of one bend
//--    h: height (Values between 5 - 15) 
//---------------------------------------------------------
void OttoWheels::ascendingTurn(float steps, int T, int h){
  // OttoWheels can't dance, he just pulls up his pants
}


//---------------------------------------------------------
//-- OttoWheels gait: Moonwalker. OttoWheels moves like Michael Jackson
//--  Parameters:
//--    Steps: Number of steps
//--    T: Period
//--    h: Height. Typical valures between 15 and 40
//--    dir: Direction: LEFT / RIGHT
//---------------------------------------------------------
void OttoWheels::moonwalker(float steps, int T, int h, int dir){
  // OttoWheels can't moonwalker
}


//----------------------------------------------------------
//-- OttoWheels gait: Crusaito. A mixture between moonwalker and walk
//--   Parameters:
//--     steps: Number of steps
//--     T: Period
//--     h: height (Values between 20 - 50)
//--     dir:  Direction: LEFT / RIGHT
//-----------------------------------------------------------
void OttoWheels::crusaito(float steps, int T, int h, int dir){
  // OttoWheels has protection charms
}


//---------------------------------------------------------
//-- OttoWheels gait: Flapping
//--  Parameters:
//--    steps: Number of steps
//--    T: Period
//--    h: height (Values between 10 - 30)
//--    dir: direction: FOREWARD, BACKWARD
//---------------------------------------------------------
void OttoWheels::flapping(float steps, int T, int h, int dir){
  // OttoWheels doesn't understand
}






///////////////////////////////////////////////////////////////////
//-- SENSORS FUNCTIONS  -----------------------------------------//
///////////////////////////////////////////////////////////////////

//---------------------------------------------------------
//-- OttoWheels getDistance: return OttoWheels's ultrasonic sensor measure
//---------------------------------------------------------
float OttoWheels::getDistance(){

  return us.read();
}


//---------------------------------------------------------
//-- OttoWheels getNoise: return Otto's noise sensor measure
//---------------------------------------------------------
int OttoWheels::getNoise(){

  int noiseLevel = 0;
  int noiseReadings = 0;
  int numReadings = 2;  

    noiseLevel = analogRead(pinNoiseSensor);

    for(int i=0; i<numReadings; i++){
        noiseReadings += analogRead(pinNoiseSensor);
        delay(4); // delay in between reads for stability
    }

    noiseLevel = noiseReadings / numReadings;

    return noiseLevel;
}


//---------------------------------------------------------
//-- OttoWheels getBatteryLevel: return battery voltage percent
//---------------------------------------------------------
double OttoWheels::getBatteryLevel(){

  //The first read of the batery is often a wrong reading, so we will discard this value. 
    double batteryLevel = battery.readBatPercent();
    double batteryReadings = 0;
    int numReadings = 10;

    for(int i=0; i<numReadings; i++){
        batteryReadings += battery.readBatPercent();
        delay(1); // delay in between reads for stability
    }

    batteryLevel = batteryReadings / numReadings;

    return batteryLevel;
}


double OttoWheels::getBatteryVoltage(){

  //The first read of the batery is often a wrong reading, so we will discard this value. 
    double batteryLevel = battery.readBatVoltage();
    double batteryReadings = 0;
    int numReadings = 10;

    for(int i=0; i<numReadings; i++){
        batteryReadings += battery.readBatVoltage();
        delay(1); // delay in between reads for stability
    }

    batteryLevel = batteryReadings / numReadings;

    return batteryLevel;
}


///////////////////////////////////////////////////////////////////
//-- MOUTHS & ANIMATIONS ----------------------------------------//
///////////////////////////////////////////////////////////////////

unsigned long int OttoWheels::getMouthShape(int number){
  unsigned long int types []={zero_code,one_code,two_code,three_code,four_code,five_code,six_code,seven_code,eight_code,
  nine_code,smile_code,happyOpen_code,happyClosed_code,heart_code,bigSurprise_code,smallSurprise_code,tongueOut_code,
  vamp1_code,vamp2_code,lineMouth_code,confused_code,diagonal_code,sad_code,sadOpen_code,sadClosed_code,
  okMouth_code, xMouth_code,interrogation_code,thunder_code,culito_code,angry_code};

  return types[number];
}


unsigned long int OttoWheels::getAnimShape(int anim, int index){

  unsigned long int littleUuh_code[]={
     0b00000000000000001100001100000000,
     0b00000000000000000110000110000000,
     0b00000000000000000011000011000000,
     0b00000000000000000110000110000000,
     0b00000000000000001100001100000000,
     0b00000000000000011000011000000000,
     0b00000000000000110000110000000000,
     0b00000000000000011000011000000000  
  };

  unsigned long int dreamMouth_code[]={
     0b00000000000000000000110000110000,
     0b00000000000000010000101000010000,  
     0b00000000011000100100100100011000,
     0b00000000000000010000101000010000           
  };

  unsigned long int adivinawi_code[]={
     0b00100001000000000000000000100001,
     0b00010010100001000000100001010010,
     0b00001100010010100001010010001100,
     0b00000000001100010010001100000000,
     0b00000000000000001100000000000000,
     0b00000000000000000000000000000000
  };

  unsigned long int wave_code[]={
     0b00001100010010100001000000000000,
     0b00000110001001010000100000000000,
     0b00000011000100001000010000100000,
     0b00000001000010000100001000110000,
     0b00000000000001000010100100011000,
     0b00000000000000100001010010001100,
     0b00000000100000010000001001000110,
     0b00100000010000001000000100000011,
     0b00110000001000000100000010000001,
     0b00011000100100000010000001000000    
  };

  switch  (anim){

    case littleUuh:
        return littleUuh_code[index];
        break;
    case dreamMouth:
        return dreamMouth_code[index];
        break;
    case adivinawi:
        return adivinawi_code[index];
        break;
    case wave:
        return wave_code[index];
        break;    
  }   
}


void OttoWheels::putAnimationMouth(unsigned long int aniMouth, int index){

      ledmatrix.writeFull(getAnimShape(aniMouth,index));
}


void OttoWheels::putMouth(unsigned long int mouth, bool predefined){

  if (predefined){
    ledmatrix.writeFull(getMouthShape(mouth));
  }
  else{
    ledmatrix.writeFull(mouth);
  }
}


void OttoWheels::clearMouth(){

  ledmatrix.clearMatrix();
}


///////////////////////////////////////////////////////////////////
//-- SOUNDS -----------------------------------------------------//
///////////////////////////////////////////////////////////////////

void OttoWheels::_tone (float noteFrequency, long noteDuration, int silentDuration){

    // tone(10,261,500);
    // delay(500);

      if(silentDuration==0){silentDuration=1;}

      tone(OttoWheels::pinBuzzer, noteFrequency, noteDuration);
      delay(noteDuration);       //milliseconds to microseconds
      //noTone(PIN_Buzzer);
      delay(silentDuration);     
}


void OttoWheels::bendTones (float initFrequency, float finalFrequency, float prop, long noteDuration, int silentDuration){

  //Examples:
  //  bendTones (880, 2093, 1.02, 18, 1);
  //  bendTones (note_A5, note_C7, 1.02, 18, 0);

  if(silentDuration==0){silentDuration=1;}

  if(initFrequency < finalFrequency)
  {
      for (int i=initFrequency; i<finalFrequency; i=i*prop) {
          _tone(i, noteDuration, silentDuration);
      }

  } else{

      for (int i=initFrequency; i>finalFrequency; i=i/prop) {
          _tone(i, noteDuration, silentDuration);
      }
  }
}


void OttoWheels::sing(int songName){
  switch(songName){

    case S_connection:
      _tone(note_E5,50,30);
      _tone(note_E6,55,25);
      _tone(note_A6,60,10);
    break;

    case S_disconnection:
      _tone(note_E5,50,30);
      _tone(note_A6,55,25);
      _tone(note_E6,50,10);
    break;

    case S_buttonPushed:
      bendTones (note_E6, note_G6, 1.03, 20, 2);
      delay(30);
      bendTones (note_E6, note_D7, 1.04, 10, 2);
    break;

    case S_mode1:
      bendTones (note_E6, note_A6, 1.02, 30, 10);  //1318.51 to 1760
    break;

    case S_mode2:
      bendTones (note_G6, note_D7, 1.03, 30, 10);  //1567.98 to 2349.32
    break;

    case S_mode3:
      _tone(note_E6,50,100); //D6
      _tone(note_G6,50,80);  //E6
      _tone(note_D7,300,0);  //G6
    break;

    case S_surprise:
      bendTones(800, 2150, 1.02, 10, 1);
      bendTones(2149, 800, 1.03, 7, 1);
    break;

    case S_OhOoh:
      bendTones(880, 2000, 1.04, 8, 3); //A5 = 880
      delay(200);

      for (int i=880; i<2000; i=i*1.04) {
           _tone(note_B5,5,10);
      }
    break;

    case S_OhOoh2:
      bendTones(1880, 3000, 1.03, 8, 3);
      delay(200);

      for (int i=1880; i<3000; i=i*1.03) {
          _tone(note_C6,10,10);
      }
    break;

    case S_cuddly:
      bendTones(700, 900, 1.03, 16, 4);
      bendTones(899, 650, 1.01, 18, 7);
    break;

    case S_sleeping:
      bendTones(100, 500, 1.04, 10, 10);
      delay(500);
      bendTones(400, 100, 1.04, 10, 1);
    break;

    case S_happy:
      bendTones(1500, 2500, 1.05, 20, 8);
      bendTones(2499, 1500, 1.05, 25, 8);
    break;

    case S_superHappy:
      bendTones(2000, 6000, 1.05, 8, 3);
      delay(50);
      bendTones(5999, 2000, 1.05, 13, 2);
    break;

    case S_happy_short:
      bendTones(1500, 2000, 1.05, 15, 8);
      delay(100);
      bendTones(1900, 2500, 1.05, 10, 8);
    break;

    case S_sad:
      bendTones(880, 669, 1.02, 20, 200);
    break;

    case S_confused:
      bendTones(1000, 1700, 1.03, 8, 2); 
      bendTones(1699, 500, 1.04, 8, 3);
      bendTones(1000, 1700, 1.05, 9, 10);
    break;

    case S_fart1:
      bendTones(1600, 3000, 1.02, 2, 15);
    break;

    case S_fart2:
      bendTones(2000, 6000, 1.02, 2, 20);
    break;

    case S_fart3:
      bendTones(1600, 4000, 1.02, 2, 20);
      bendTones(4000, 3000, 1.02, 2, 20);
    break;

  }
}



///////////////////////////////////////////////////////////////////
//-- GESTURES ---------------------------------------------------//
///////////////////////////////////////////////////////////////////

void OttoWheels::playGesture(int gesture){

  int sadPos[4]=      {110, 70, 20, 160};
  int bedPos[4]=      {100, 80, 60, 120};
  int fartPos_1[4]=   {90, 90, 145, 122}; //rightBend
  int fartPos_2[4]=   {90, 90, 80, 122};
  int fartPos_3[4]=   {90, 90, 145, 80};
  int confusedPos[4]= {110, 70, 90, 90};
  int angryPos[4]=    {90, 90, 70, 110};
  int headLeft[4]=    {110, 110, 90, 90};
  int headRight[4]=   {70, 70, 90, 90};
  int fretfulPos[4]=  {90, 90, 90, 110};
  int bendPos_1[4]=   {90, 90, 70, 35};
  int bendPos_2[4]=   {90, 90, 55, 35};
  int bendPos_3[4]=   {90, 90, 42, 35};
  int bendPos_4[4]=   {90, 90, 34, 35};
  
  switch(gesture){

    case OttoHappy: 
        _tone(note_E5,50,30);
        putMouth(smile);
        sing(S_happy_short);
        swing(1,800,20); 
        sing(S_happy_short);

        home();
        putMouth(happyOpen);
    break;


    case OttoSuperHappy:
        putMouth(happyOpen);
        sing(S_happy);
        putMouth(happyClosed);
        tiptoeSwing(1,500,20);
        putMouth(happyOpen);
        sing(S_superHappy);
        putMouth(happyClosed);
        tiptoeSwing(1,500,20); 

        home();  
        putMouth(happyOpen);
    break;


    case OttoSad: 
        putMouth(sad);
        _moveMotors(700, sadPos);     
        bendTones(880, 830, 1.02, 20, 200);
        putMouth(sadClosed);
        bendTones(830, 790, 1.02, 20, 200);  
        putMouth(sadOpen);
        bendTones(790, 740, 1.02, 20, 200);
        putMouth(sadClosed);
        bendTones(740, 700, 1.02, 20, 200);
        putMouth(sadOpen);
        bendTones(700, 669, 1.02, 20, 200);
        putMouth(sad);
        delay(500);

        home();
        delay(300);
        putMouth(happyOpen);
    break;


    case OttoSleeping:
        _moveMotors(700, bedPos);     

        for(int i=0; i<4;i++){
          putAnimationMouth(dreamMouth,0);
          bendTones (100, 200, 1.04, 10, 10);
          putAnimationMouth(dreamMouth,1);
          bendTones (200, 300, 1.04, 10, 10);  
          putAnimationMouth(dreamMouth,2);
          bendTones (300, 500, 1.04, 10, 10);   
          delay(500);
          putAnimationMouth(dreamMouth,1);
          bendTones (400, 250, 1.04, 10, 1); 
          putAnimationMouth(dreamMouth,0);
          bendTones (250, 100, 1.04, 10, 1); 
          delay(500);
        } 

        putMouth(lineMouth);
        sing(S_cuddly);

        home();  
        putMouth(happyOpen);
    break;


    case OttoFart:
        _moveMotors(500,fartPos_1);
        delay(300);     
        putMouth(lineMouth);
        sing(S_fart1);  
        putMouth(tongueOut);
        delay(250);
        _moveMotors(500,fartPos_2);
        delay(300);
        putMouth(lineMouth);
        sing(S_fart2); 
        putMouth(tongueOut);
        delay(250);
        _moveMotors(500,fartPos_3);
        delay(300);
        putMouth(lineMouth);
        sing(S_fart3);
        putMouth(tongueOut);    
        delay(300);

        home(); 
        delay(500); 
        putMouth(happyOpen);
    break;


    case OttoConfused:
        _moveMotors(300, confusedPos); 
        putMouth(confused);
        sing(S_confused);
        delay(500);

        home();  
        putMouth(happyOpen);
    break;


    case OttoLove:
        putMouth(heart);
        sing(S_cuddly);
        crusaito(2,1500,15,1);

        home(); 
        sing(S_happy_short);  
        putMouth(happyOpen);
    break;


    case OttoAngry: 
        _moveMotors(300, angryPos); 
        putMouth(angry);

        _tone(note_A5,100,30);
        bendTones(note_A5, note_D6, 1.02, 7, 4);
        bendTones(note_D6, note_G6, 1.02, 10, 1);
        bendTones(note_G6, note_A5, 1.02, 10, 1);
        delay(15);
        bendTones(note_A5, note_E5, 1.02, 20, 4);
        delay(400);
        _moveMotors(200, headLeft); 
        bendTones(note_A5, note_D6, 1.02, 20, 4);
        _moveMotors(200, headRight); 
        bendTones(note_A5, note_E5, 1.02, 20, 4);

        home();  
        putMouth(happyOpen);
    break;


    case OttoFretful: 
        putMouth(angry);
        bendTones(note_A5, note_D6, 1.02, 20, 4);
        bendTones(note_A5, note_E5, 1.02, 20, 4);
        delay(300);
        putMouth(lineMouth);

        for(int i=0; i<4; i++){
          _moveMotors(100, fretfulPos);   
          home();
        }

        putMouth(angry);
        delay(500);

        home();  
        putMouth(happyOpen);
    break;


    case OttoMagic:

        //Initial note frecuency = 400
        //Final note frecuency = 1000
        
        // Reproduce the animation four times
        for(int i = 0; i<4; i++){ 

          int noteM = 400; 

            for(int index = 0; index<6; index++){
              putAnimationMouth(adivinawi,index);
              bendTones(noteM, noteM+100, 1.04, 10, 10);    //400 -> 1000 
              noteM+=100;
            }

            clearMouth();
            bendTones(noteM-100, noteM+100, 1.04, 10, 10);  //900 -> 1100

            for(int index = 0; index<6; index++){
              putAnimationMouth(adivinawi,index);
              bendTones(noteM, noteM+100, 1.04, 10, 10);    //1000 -> 400 
              noteM-=100;
            }
        } 
 
        delay(300);
        putMouth(happyOpen);
    break;


    case OttoWave:
        
        // Reproduce the animation four times
        for(int i = 0; i<2; i++){ 

            int noteW = 500; 

            for(int index = 0; index<10; index++){
              putAnimationMouth(wave,index);
              bendTones(noteW, noteW+100, 1.02, 10, 10); 
              noteW+=101;
            }
            for(int index = 0; index<10; index++){
              putAnimationMouth(wave,index);
              bendTones(noteW, noteW+100, 1.02, 10, 10); 
              noteW+=101;
            }
            for(int index = 0; index<10; index++){
              putAnimationMouth(wave,index);
              bendTones(noteW, noteW-100, 1.02, 10, 10); 
              noteW-=101;
            }
            for(int index = 0; index<10; index++){
              putAnimationMouth(wave,index);
              bendTones(noteW, noteW-100, 1.02, 10, 10); 
              noteW-=101;
            }
        }    

        clearMouth();
        delay(100);
        putMouth(happyOpen);
    break;

    case OttoVictory:
        
        putMouth(smallSurprise);
        //final pos   = {90,90,150,30}
        for (int i = 0; i < 60; ++i){
          int pos[]={90,90,90+i,90-i};  
          _moveMotors(10,pos);
          _tone(1600+i*20,15,1);
        }

        putMouth(bigSurprise);
        //final pos   = {90,90,90,90}
        for (int i = 0; i < 60; ++i){
          int pos[]={90,90,150-i,30+i};  
          _moveMotors(10,pos);
          _tone(2800+i*20,15,1);
        }

        putMouth(happyOpen);
        //SUPER HAPPY
        //-----
        tiptoeSwing(1,500,20);
        sing(S_superHappy);
        putMouth(happyClosed);
        tiptoeSwing(1,500,20); 
        //-----

        home();
        clearMouth();
        putMouth(happyOpen);

    break;

    case OttoFail:

        putMouth(sadOpen);
        _moveMotors(300,bendPos_1);
        _tone(900,200,1);
        putMouth(sadClosed);
        _moveMotors(300,bendPos_2);
        _tone(600,200,1);
        putMouth(confused);
        _moveMotors(300,bendPos_3);
        _tone(300,200,1);
        _moveMotors(300,bendPos_4);
        putMouth(xMouth);

        _tone(150,2200,1);
        
        delay(600);
        clearMouth();
        putMouth(happyOpen);
        home();

    break;

  }
}    
