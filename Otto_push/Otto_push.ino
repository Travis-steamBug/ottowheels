//----------------------------------------------------------------
//-- CC BY SA (http://ottodiy.com)
//-----------------------------------------------------------------
//-- Otto will be "push" by hand or other object
//-- change to Otto.walk(1,1000,1) to make Otto follow you!
//-----------------------------------------------------------------
#include <US.h>
#include <OttoWheels.h>

#define LEFT_MOTOR 3
#define RIGHT_MOTOR 4

OttoWheels Otto;  //This is Otto!

int turnCount = 3;
int turnDirection = RIGHT;

//---------------------------------------------------------
//-- Two DC motors are attached to motor ports 3 & 4 on the
//-- L293D motor shield
//---------------------------------------------------------
/*
             --------------- 
            |     O   O     |
            |---------------|
           ||               ||
Motor 3 ==>| ------  ------  | <== Motor 4
           |                 |
*/

// buzzer is connected to pin A3

///////////////////////////////////////////////////////////////////
//-- Global Variables -------------------------------------------//
///////////////////////////////////////////////////////////////////
//-- Movement parameters
int T=1000;              //Initial duration of movement
int moveId=0;            //Number of movement
int moveSize=15;         //Asociated with the height of some movements
///////////////////////////////////////////////////////////////////
//-- Setup ------------------------------------------------------//
///////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(9600);
  //Set the motor pins
  Otto.init(LEFT_MOTOR, RIGHT_MOTOR, true);
  Otto.sing(S_connection); //Otto wake up!
  Otto.home();
  delay(50);
  Otto.sing(S_happy); // a happy Otto :)
}
///////////////////////////////////////////////////////////////////
//-- Principal Loop ---------------------------------------------//
///////////////////////////////////////////////////////////////////
void loop() {
  if(obstacleDetector()){ 
    Otto.walk(1,500,1);  //Otto.walk(1,1000,1) to make Otto follow you!
  } else {
    if(turnCount < 6) {
      Otto.turn(0.070, 50, turnDirection);
      delay(150); 
      turnCount++;
    } else {
      if(turnDirection == RIGHT) {
        turnDirection = LEFT;
      } else {
        turnDirection = RIGHT;
      }
      turnCount = 0;
    }
    Otto.home();
    delay(250);
  }           
}
///////////////////////////////////////////////////////////////////
//-- Function to read distance sensor & to actualize obstacleDetected variable
bool obstacleDetector() {
  int distance = Otto.getDistance();
  if(distance<25) {
    return true;
  } else {
    return false;
  }
}
